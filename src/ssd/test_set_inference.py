import numpy as np
import os
import cv2

from PIL import Image
from keras.preprocessing import image
from keras.optimizers import Adam
# from imageio import imread
# from yolo import YOLO
import csv
import glob


from keras.models import load_model
from keras import backend as K
from keras.optimizers import Adam
from keras.models import model_from_json

from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras_layers.keras_layer_L2Normalization import L2Normalization

from ssd_encoder_decoder.ssd_output_decoder import decode_detections, decode_detections_fast

# from data_generator.object_detection_2d_data_generator import DataGenerator
# from data_generator.object_detection_2d_photometric_ops import ConvertTo3Channels
# from data_generator.object_detection_2d_geometric_ops import Resize
# from data_generator.object_detection_2d_misc_utils import apply_inverse_transforms



class TestSetInference:

    labels_mapping = {
        "vehicle" : "0",
        "pedestrian" : "1"
    }

    def __init__(self, 
                test_set_folder, 
                detections_output_file, 
                test_set_images=None,
                score=0.1,
                iou=0.1,
                weights=None, 
                anchors=None, 
                labels=None,
                ):
        self.model_path = weights
        self.test_set_folder = test_set_folder
        self.detections_output_file = detections_output_file
        self.test_set_images = test_set_images

# -----------------------------------------------------------------------

        # Set the image size.
        img_height = 1224
        img_width = 370

        # 1: Build the Keras model

        K.clear_session() # Clear previous models from memory.

        model = ssd_300(image_size=(img_height, img_width, 3),
                        n_classes=20,
                        mode='inference',
                        l2_regularization=0.0005,
                        scales=[0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05], # The scales for MS COCO are [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05]
                        aspect_ratios_per_layer=[[1.0, 2.0, 0.5],
                                                [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                                [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                                [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                                [1.0, 2.0, 0.5],
                                                [1.0, 2.0, 0.5]],
                        two_boxes_for_ar1=True,
                        steps=[8, 16, 32, 64, 100, 300],
                        offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                        clip_boxes=False,
                        variances=[0.1, 0.1, 0.2, 0.2],
                        normalize_coords=True,
                        subtract_mean=[123, 117, 104],
                        swap_channels=[2, 1, 0],
                        confidence_thresh=0.5,
                        iou_threshold=0.45,
                        top_k=200,
                        nms_max_output_size=400)
        weights_path = self.model_path
        model.load_weights(weights_path, by_name=True)
        adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
        ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)

        model.compile(optimizer=adam, loss=ssd_loss.compute_loss)
        
        # model_json = model.to_json()
        # with open("model.json", "w") as json_file:
        #     json_file.write(model_json)
        # # load json and create model
        # json_file = open('model.json', 'r')
        # loaded_model_json = json_file.read()
        # json_file.close()
        # loaded_model = model_from_json(loaded_model_json)
        # # load weights into new model
        # loaded_model.load_weights(self.model_path)


 # ----------------------------------------------------------------------       
        # model_path = self.model_path

        # # We need to create an SSDLoss object in order to pass that to the model loader.
        # ssd_loss = SSDLoss(neg_pos_ratio=3, n_neg_min=0, alpha=1.0)

        # K.clear_session() # Clear previous models from memory.

        # model = load_model(model_path, custom_objects={'AnchorBoxes': AnchorBoxes,
        #                                             'L2Normalization': L2Normalization,
        #                                             'DecodeDetections': DecodeDetections,
        #                                             'compute_loss': ssd_loss.compute_loss})
# ---------------------------------------------------------------------------

        self.yolo = model
        self.seq = 0

    def detect_single_image(self, image_path, display=False, image_name=None, save_image_folder=None):
        self.seq += 1
        # image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
        # image_pil = Image.fromarray(image_)
        orig_images = []
        input_image = []
        # orig_images.append(imread(image))
        img = image.load_img(image_path, target_size=(1224, 370))
        img = image.img_to_array(img) 
        input_image.append(img)
        input_image = np.array(input_image)
        # print(input_image)
# --------------------------------------------------------------------------------------------------
        
        # objects = self.yolo.get_detections(image_pil)
        objects = self.yolo.predict(input_image)
        # print('printing objects')
        # print(objects)
        confidence_threshold = 0.1

        y_pred_thresh = [objects[k][objects[k,:,1] > confidence_threshold] for k in range(objects.shape[0])]
# ---------------------------------------------------------------------------------------------------
        # if display:
        #     r_image = self.yolo.detect_image(image_pil)
        #     if save_image_folder and image_name:
        #         r_image.save(os.path.join(save_image_folder, image_name))
# -----------------------------------------------------------------------------------------------------
            # cv_image = cv2.cvtColor(np.array(r_image), cv2.COLOR_RGB2BGR)
            # cv2.imshow("Detected Objects", cv_image)
            # cv2.waitKey(1)

# ------------------------------------------------------------------------------------------------------





# --------------------------------------------------------------------------------------------------------
        return_list = ""
        print('y_pred'+str(y_pred_thresh[0]))
        for obj in y_pred_thresh[0]:
            if obj[0] == 7:
                xmin = obj[2]
                ymin = obj[3]
                xmax = obj[4]
                ymax = obj[5]
                score = obj[1]
                label = obj[0]
                obj_str = str(xmin) + ',' + str(ymin) + ',' + str(xmax) + ',' + str(ymax) + ',' + str(score) + ',' + str(label) + ' '
                return_list += obj_str
        # print(return_list)
        return return_list
    
    def run_inference(self, save_folder=None, ext='.jpg'):
        # Get a list of images
        files = []
        if self.test_set_images is not None:
            with open(self.test_set_images, 'r') as f_in:
                for count, line in enumerate(f_in):
                    image_name = line.split(' ')[0]
                    files.append(os.path.join(self.test_set_folder, image_name + ext))
        else:
            types = ['*.jpg', '*.png']
            for t in types:
                print(self.test_set_folder)
                files.extend(glob.glob(self.test_set_folder + t))
        files.sort()
        print("Files found: " + str(len(files)))

        # Get detections for each image
        for file in files:
            fn = os.path.basename(file)
            image = cv2.imread(file)
            # Output detections to the save_folder
# ------------------------------------------------------------------------------------
            dets = self.detect_single_image(file, display=True, image_name=fn, save_image_folder=save_folder)
# ------------------------------------------------------------------------------------
            # Append to CSV file
            print(fn, dets)
            with open(self.detections_output_file, 'a', newline = '') as f_out:
                wr = csv.writer(f_out, delimiter=';')
                wr.writerow([fn, dets])
            
            print("Processed " + str(self.seq) + " images")


#### Example 1: On single image
# test_set_folder = ""
# detections_output_file = ""
# weights = 'logs/lgsvl_2_dataset_1/000ep039-loss16.616-val_loss17.654.h5'
# anchors = 'model_data/yolo_lgsvl_2_anchors.txt'
# labels = 'model_data/yolo_lgsvl_2_labels.txt'
# score = 0.05
# iou = 0.01

# tsi = TestSetInference(test_set_folder, 
#                         detections_output_file, 
#                         weights=weights, 
#                         anchors=anchors, 
#                         labels=labels, 
#                         score=score, 
#                         iou=iou)

# test_image_folder = "test_folder"
# test_image_name = 'test4.jpg'

# test_image_path = os.path.join(test_image_folder, test_image_name)
# det_image_path = os.path.join(test_image_folder, "detections")
# # test_image = "/home/deepaktalwardt/Dropbox/SJSU/Semesters/Fall2019/CMPE256/Project/ObjectDetection/training1/keras-yolo3/test_folder/test2.jpg"

# t_image = cv2.imread(test_image_path)
# det_obj_str = tsi.detect_single_image(t_image, display=True, image_name=test_image_name, save_image_folder=det_image_path)

#### Example 2: On sample testset
# test_set_folder = "../../sample_testset/images/"
# detections_output_file = "../detections/sample_testset/detections_lgsvl_2.csv"
# weights = 'logs/lgsvl_2_dataset_3/000ep078-loss9.684-val_loss9.917.h5'
# anchors = 'model_data/yolo_no_ped_anchors.txt'
# labels = 'model_data/yolo_no_ped_labels.txt'
# score = 0.25
# iou = 0.5

# tsi = TestSetInference(test_set_folder, 
#                         detections_output_file, 
#                         weights=weights, 
#                         anchors=anchors, 
#                         labels=labels, 
#                         score=score, 
#                         iou=iou)

# detections_folder = '../detections/sample_testset/images'
# tsi.run_inference(save_folder=detections_folder)

#### Example 3: On entire Waymo testset
# test_set_folder = "../../waymo_testset/images/"
# detections_output_file = "../detections/waymo_testset/detections_lgsvl_2_waymo_testset.csv"
# weights = 'logs/lgsvl_2_dataset_3/000ep078-loss9.684-val_loss9.917.h5'
# anchors = 'model_data/yolo_no_ped_anchors.txt'
# labels = 'model_data/yolo_no_ped_labels.txt'
# test_set_images = "../../waymo_testset/waymo_testset_1000_sorted_iou_2.txt"
# score = 0.1
# iou = 0.1

# tsi1 = TestSetInference(test_set_folder, 
#                         detections_output_file,
#                         test_set_images=test_set_images,
#                         weights=weights, 
#                         anchors=anchors, 
#                         labels=labels, 
#                         score=score, 
#                         iou=iou)

# detections_folder = '../detections/waymo_testset/images'
# tsi1.run_inference(save_folder=detections_folder)


#### Example 4: On entire KITTI dataset using KITTI weights
# test_set_folder = "../../kitti/training/image_2/"
# detections_output_file = "../kitti_detections/detections_kitti_kitti_training_set.csv"
# detections_folder = '../kitti_detections/images'
# weights = 'logs/kitti_training_weights/kitti_epoch_final_30iter_unfreeze_all.h5'
# anchors = 'model_data/kitti_training_anchors.txt'
# labels = 'model_data/yolo_no_ped_labels.txt'
# score = 0.1
# iou = 0.1

# tsi2 = TestSetInference(test_set_folder, 
#                         detections_output_file, 
#                         weights=weights, 
#                         anchors=anchors, 
#                         labels=labels, 
#                         score=score, 
#                         iou=iou)

# tsi2.run_inference(save_folder=detections_folder)

#### Example 5: On top 1000 KITTI dataset using KITTI weights
test_set_folder = "temp/testImages_KITTI/"
# test_set_images = '../../kitti/kitti_testset_1000_iou_2.txt'
detections_output_file = "temp/detections_KITTI/outputFile.csv"
detections_folder = 'temp/detections/detectionsImages'
weights = 'temp/VGG_VOC0712_SSD_300x300_ft_iter_120000.h5'
anchors = None
labels = None
score = 0.1
iou = 0.1

tsi2 = TestSetInference(test_set_folder, 
                        detections_output_file, 
                        # test_set_images=test_set_images,
                        score=score,
                        iou=iou,
                        weights=weights, 
                        anchors=anchors, 
                        labels=labels
                        )

tsi2.run_inference(save_folder=detections_folder, ext='.png')


### Example 6: Using LGSVL 2
# test_set_folder = "../../kitti/training/image_2/"
# test_set_images = '../../kitti/kitti_testset_1000_iou_2.txt'
# detections_output_file = "../detections/lgsvl_2_ON_kitti_testset/detections_lgsvl_2_kitti_testset.csv"
# detections_folder = '../detections/lgsvl_2_ON_kitti_testset/images'
# weights = 'logs/lgsvl_2_dataset_3/000ep078-loss9.684-val_loss9.917.h5'
# anchors = 'model_data/yolo_no_ped_anchors.txt'
# labels = 'model_data/yolo_no_ped_labels.txt'
# score = 0.15
# iou = 0.2

# tsi1 = TestSetInference(test_set_folder, 
#                         detections_output_file,
#                         test_set_images=test_set_images,
#                         weights=weights, 
#                         anchors=anchors, 
#                         labels=labels, 
#                         score=score, 
#                         iou=iou)

# tsi1.run_inference(save_folder=detections_folder, ext='.png')