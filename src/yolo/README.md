# CMPE 295 - Spring 2020 Project

The original implementation of YOLOv3 comes from [qqwweee/keras-yolo3](https://github.com/qqwweee/keras-yolo3). This repository contains the modified implementation for the datasets mentioned below and in the project report. This repository also contains code for data collection from LGSVL Simulators written as ROS (Robot Operating System) Nodes, as well as data processing and evaluation scripts.

## Introduction
In this project, we aim to determine if data generated from simulated 3D environments can be generalized to real-world test datasets. Here, we provide code for the following tasks:
* Collecting data from the simulation environments.
* Extracting data from Waymo Open Dataset and converting it to our format.
* Extracting data from KITTI Vision benchmark dataset and converting it to our format.
* Training YOLOv3 Object detection algorithm on these datasets.
* Running inference of trained models on test datasets.
* Evaluating performance of inference using the mean average precision (mAP).

## Data Collection
### Simulated Datasets
For this project, we collected data from two different versions of LGSVL Automotive Simulator by writing ROS Nodes and connecting to the simulators over ROS Bridge. For more details on how the LGSVL Simulator works please visit the links below

#### [LGSVL 2019.05 Release (Sim1)](https://github.com/lgsvl/simulator-2019.05-obsolete)
#### [LGSVL 2019.10 Release (Sim2)](https://github.com/lgsvl/simulator)

The ROS packages written for collected data from the above mentioned simulators are provided in [ros_workspace](ros_workspace) folder. If you wish to run them, please install ROS Kinetic Kame and run `catkin_make` to build these packages.

If you wish to download the collected datasets please download the zip files linked below

#### [LGVSL Sim1 Dataset](https://www.dropbox.com/s/nhbwk2gqulnkodi/lgsvl_1_dataset_2.zip?dl=0)
#### [LGSVL Sim2 Dataset](https://www.dropbox.com/s/zh603pabvakt2i9/lgsvl_2_dataset_3.zip?dl=0)

These zip files contain the front camera 2D images as well as the ground truth information in a single CSV file.


### KITTI Dataset: Converting KITTI dataset to YOLOV3 finput format

1. KITTI dataset is a stero camera dataset.
2. Dataset is of the format that each image contains it's individual text file which contains the class name, bounding box and other information.
3. we needed to convert it into single annotation file with bounding box, class. 
4. pass the list of annotation files of kitti dataset into [annotation_converter_kitti.py](https://github.com/saching13/Validation-of-Synthetic-Image-data-for-Autonomous-Driving/blob/master/annotation_converter_kitti.py) to create a single annotation file.

KITTI dataset can be found [here](http://www.cvlibs.net/datasets/kitti/eval_object.php?obj_benchmark=2d)

#### [image annotator.ipynb](https://github.com/saching13/Validation-of-Synthetic-Image-data-for-Autonomous-Driving/blob/master/image%20annotator.ipynb)

  On passsing the images folder path  and annotation file with bounding boxes it will generate images with bounding boxes for visualization. set `gcs = False` to create the output files in local file system and `True` to create files in GCP.

## Training
1. We generated our own annotation file and class names file according to the following format  
    One row for one image;  
    Row format: `image_file_path box1 box2 ... boxN`;  
    Box format: `x_min,y_min,x_max,y_max,class_id` (no space).  
    Here is an example:
    ```
    path/to/img1.jpg 50,100,150,200,0 30,50,200,120,3
    path/to/img2.jpg 120,300,250,600,2
    ...
    ```

2. We then calculated the correct anchors for each of our datasets using [kmeans.py](kmeans.py) script. These anchors are then placed into the anchors file provided for training.

3. To start training, we modified the [train.py](train.py) file with the correct image sizes, anchor sizes, class names, starting weights as well as hyper-parameters. We started with pretrained YOLOv3 weights. These weights can be downloaded [here](https://www.dropbox.com/s/a44ly3zd6bzmssw/2d-final-weights-keras-yolo3.h5?dl=0).

4. The progress of the trainings can be looked at through TensorBoard.

5. If you wish to use our training weights you can find them here:
* [Model trained on LGSVL Sim1 dataset](https://www.dropbox.com/s/jlevzz3yf4urbr4/lgsvl_1trained_weights_final.h5?dl=0)
* [Model trained on LGSVL Sim2 dataset](https://www.dropbox.com/s/18xi1ljwfvjlx2x/000trained_weights_final.h5?dl=0)
* [Model trained on LGSVL KITTI dataset](https://www.dropbox.com/s/edg06gy1le529ai/kitti_epoch_final_30iter_unfreeze_all.h5?dl=0)
